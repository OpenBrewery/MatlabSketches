function measurements = get_measurements(url,deviceId,beginTime,endTime)
addpath('jsonlab'); 

pkg load struct;
%download live measurements
request = sprintf('%s?deviceId=%i&fromEpochSecond=%i&toEpochSecond=%i',url,deviceId,beginTime,endTime);
measurements = urlread(request);

data=loadjson(measurements);
measurements = cell2mat(data.content);
