
deviceId = 15739288;
beginTime = 1525603860;
endTime = 1526330502;

meas = get_measurements("https://api.open-brewery.com/api/measurements",deviceId,beginTime,endTime);


%refractometer manual measurements
manual = [  1525603860,21.5,21.5;
            1525605434,21.5,21.5;
            1525616551,21.5,21.5;
            1525635523,21.0,20.3;
            1525649814,20.5,19.4;
            1525672800,19.5,17.8;
            1525712879,18.5,16.2;
            1525726487,18.0,15.7;
            1525757146,17.0,14.1;
            1525791088,16.2,12.8;
            1525814488,15.5,11.6;
            1525886169,14.0,9.3;
            1525902897,13.0,7.8;
            1525930206,12.5,7.0;
            1525971906,11.5,5.5;
            1525987352,11.2,5.0;
            1526018941,10.5,3.9;
            1526064497,10.0,3.1;
            1526081610,9.0,1.5;
            1526120130,8.5,0.7;
            1526212950,7.5,-0.9;
            1526239410,7.0,-1.7;
            1526329411,6.5,-2.5,
         ];
         
         
auto = zeros(0,3);

for i=meas
    auto = [auto;i.epochSecond,(1-i.cosFi),(1-i.cosFi)];
end

%values from 1st fermentation
offset =  1-beerbob1_0blg();
amplitude =  180


figure('Position',[0,0,1920,1000],'Name',"Sugar in water, 21,5blg fermentation, BeerBob1");
plot_blg_with_fit(manual,auto,offset,amplitude);




pause
