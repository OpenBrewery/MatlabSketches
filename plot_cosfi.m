function plot_cosfi(measurements,avgPoints) 

b(1:avgPoints) = 1/avgPoints;
a = 1;

smooth = filter(b,a,[measurements.cosFi]);

for i=[1:avgPoints]
    smooth(i) = smooth(avgPoints+1);
end

hold on;
mintime_1 = min([measurements.epochSecond]);
plot(([measurements.epochSecond]-mintime_1)./60,smooth,"linewidth",2,'b');
plot(([measurements.epochSecond]-mintime_1)./60,[measurements.cosFi],"linewidth",1.5,'bx');

xlabel("time (min)");
ylabel("cosFi");

hold off;

end
