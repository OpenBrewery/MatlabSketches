function plot_drift_compensation(measurements)

min_temp = min([measurements.temp])
max_temp = max([measurements.temp])

bins = (max_temp-min_temp)*2;

counters = zeros(1,bins);
sum = zeros(1,bins);

if bins > 0
    for i = measurements
        for j = 1:bins
            if i.temp == min_temp+j/2
                counters(j)++;
                sum(j) += i.cosFi;
            end
        end
    end

    for j=1:bins
        avg_cos(j) = sum(j) / counters(j);
        temperatures(j) = min_temp + j/2;
    end
end


hold on;
plot([measurements.cosFi],[measurements.temp],"linewidth",3,'kx')
if bins > 0
    plot(avg_cos,temperatures,"linewidth",3,'k');
end
hold off;

title("Dependency between temperature and cosFi");
xlabel("cosFi");
ylabel("temp (*C)");

end
