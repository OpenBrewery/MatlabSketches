function [avg_cos] = beerbob1_0blg()

deviceId = 15739288;
beginTime = 1525602271;
endTime = 1525603582;

meas = get_measurements("https://api.open-brewery.com/api/measurements",deviceId,beginTime,endTime);


avg_cos = mean([meas.cosFi])
min_cos = min([meas.cosFi])
max_cos = max([meas.cosFi])
