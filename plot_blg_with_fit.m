function plot_blg_with_fit(manual_meas,auto_meas,offset,amplitude)

hold on;
grid on;
mintime = min(manual_meas(:,1));


plot(([manual_meas(:,1)]-mintime)./60,[manual_meas(:,3)],"linewidth",1.5,'bx');
plot(([manual_meas(:,1)]-mintime)./60,[manual_meas(:,3)],"linewidth",1.5,'b');

%plot(([manual_meas(:,1)]-mintime)./60,[manual_meas(:,2)],"linewidth",1.5,'rx');
%plot(([manual_meas(:,1)]-mintime)./60,[manual_meas(:,2)],"linewidth",1.5,'r');

plot(([auto_meas(:,1)]-mintime)./60,[(auto_meas(:,3)-offset).*amplitude],"linewidth",1.5,'k');
%plot(([auto_meas(:,1)]-mintime)./60,[auto_meas(:,2)],"linewidth",1.5,'r');


xlabel("time (min)");
ylabel("blg(blue),brix(red)");

hold off;

end
