
deviceId = 15739288;
beginTime = 1525299600;
endTime = 1525508899;

meas = get_measurements("https://api.open-brewery.com/api/measurements",deviceId,beginTime,endTime);



%refractometer manual measurements
manual = [  1525299600,10.5,10.5;
            1525338240,8.5,7.0;
            1525350060,7.5,5.5;
            1525361580,6.5,3.9;
            1525370160,6.0,3.1;
            1525376647,5.5,2.4;
            1525385280,4.5,0.8;
            1525412568,4.0,0.0;
            1525467606,4.0,0.0;%third col is brewness correction for refractometers
            1525508899,4.0,0.0;
         ];
         
auto = zeros(0,3);

for i=meas
    auto = [auto;i.epochSecond,(1-i.cosFi),(1-i.cosFi)];
end

offset = auto(800,3) %because its 0 blg
amplitude = manual(1,3)/(auto(1,3)-offset)

figure('Position',[0,0,1920,1000],'Name',"First cider fermentation, BeerBob1");
plot_blg_with_fit(manual,auto,offset,amplitude);




pause
