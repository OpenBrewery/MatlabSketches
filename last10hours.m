function last10hours(deviceId, beginTime, endTime,name)


meas = get_measurements("https://api.open-brewery.com/api/measurements",deviceId,beginTime,endTime);


figure('Position',[0,0,1920,1000],'Name',sprintf('Last 10 hours, %s',name));
subplot(2,1,1);
plot_cosfi(meas,1);
subplot(2,1,2);
plot_temperature(meas,1);



pause
