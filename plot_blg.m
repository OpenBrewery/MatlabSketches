function plot_blg(manual_meas)

hold on;
grid on;
mintime = min(manual_meas(:,1));

plot(([manual_meas(:,1)]-mintime)./60,[manual_meas(:,3)],"linewidth",1.5,'bx');
plot(([manual_meas(:,1)]-mintime)./60,[manual_meas(:,3)],"linewidth",1.5,'b');

plot(([manual_meas(:,1)]-mintime)./60,[manual_meas(:,2)],"linewidth",1.5,'rx');
plot(([manual_meas(:,1)]-mintime)./60,[manual_meas(:,2)],"linewidth",1.5,'r');


xlabel("time (min)");
ylabel("blg(blue),brix(red)");

hold off;

end
