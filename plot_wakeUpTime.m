function plot_wakeUpTime(measurements,avgPoints)

b(1:avgPoints) = 1/avgPoints;
a = 1;

smooth = filter(b,a,[measurements.wakeUpTime]);

for i=[1:avgPoints]
    smooth(i) = smooth(avgPoints+1);
end

hold on;
mintime_1 = min([measurements.epochSecond]);
plot(([measurements.epochSecond]-mintime_1)./60,smooth,"linewidth",2,'g');
plot(([measurements.epochSecond]-mintime_1)./60,[measurements.wakeUpTime],"linewidth",1.5,'gx');

xlabel("time (min)");
ylabel("previous wakeUpTime (ms)");

hold off;

end
