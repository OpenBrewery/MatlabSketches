function plot_temperature(measurements,avgPoints)

b(1:avgPoints) = 1/avgPoints;
a = 1;

smooth = filter(b,a,[measurements.temp]);

for i=[1:avgPoints]
    smooth(i) = smooth(avgPoints+1);
end

mintime_1 = min([measurements.epochSecond]);
hold on;
plot(([measurements.epochSecond]-mintime_1)./60,smooth,"linewidth",2,'r');
plot(([measurements.epochSecond]-mintime_1)./60,[measurements.temp],"linewidth",1.5,'rx');
hold off;
xlabel("time (min)");
ylabel("temp (*C)");

end
