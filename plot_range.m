function plot_range(plot_title,measurements)

figure('Position',[0,0,1920,1000],'Name',plot_title);
subplot(2,2,1);
plot_cosfi(measurements,5);
subplot(2,2,2);
plot_temperature(measurements,2);
subplot(2,2,3:4);
plot_drift_compensation(measurements);


end
